Embedded Systems coursework. Includes low-level programming of a safety alarm system with a basic OS on the hardware, and the same functionality replicated in bare-metal programming.