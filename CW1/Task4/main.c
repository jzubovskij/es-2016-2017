/**HEADER*******************************************************************
 * * 
 * * Copyright (c) 2008 Freescale Semiconductor;
 * * All Rights Reserved
 * *
 * * Copyright (c) 1989-2008 ARC International;
 * * All Rights Reserved
 * *
 * **************************************************************************** 
 * *
 * * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
 * * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
 * * IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * * THE POSSIBILITY OF SUCH DAMAGE.
 * *
 * ****************************************************************************
 * *
 * * Comments:
 * *
 * *   This file contains main initialization for your application
 * *   and infinite loop
 * *
 * *END************************************************************************/

#include "main.h"
#include <tfs.h>
#include <httpd.h>
#include <httpd_types.h>
#include <stdlib.h>

#if defined(APPLICATION_HAS_SHELL) && (!SHELLCFG_USES_RTCS)
#error This application requires SHELLCFG_USES_RTCS defined non-zero in user_config.h. Please recompile libraries with this option if any Ethernet interface is available.
#endif

TASK_TEMPLATE_STRUCT MQX_template_list[] =
{
/*  Task number, Entry point, Stack, Pri, String, Auto? */
   {MAIN_TASK,   Main_task,   2000,  9,   "main", MQX_AUTO_START_TASK},
   {0,           0,           0,     0,   0,      0,                 }
};

/*TASK*-----------------------------------------------------------------
 * *
 * * Function Name  : Main_task
 * * Comments       :
 * *    This task initializes MFS and starts SHELL.
 * *
 * *END------------------------------------------------------------------*/

int sensors_triggered[4] = {0,0,0,0};
int sensors_enabled[4] = {1,1,1,1};
int alarm_on = 0;
int alarm_hushed = 0;

static int sensor_enable_time[4] = {-1,-1,-1,-1};
static int sensor_disable_time[4] = {-1,-1,-1,-1};

////////// HTML/JS SPECIFICATION OF THE WEBSITE

unsigned char http_refresh_text[] = "<!DOCTYPE html>"
"<html>"
"<head>"
"<title>Embedded Systems</title>"
"</head>"
"<body>"
"<h1>Tower Control</h1>"

// TIME OPTIONS

"TIME OPTIONS: Select desired option, enter new time(if appliccable) and click corresponding button"
"<br>"

"  	<input type=\"submit\" onclick=\"location.href='http://192.168.105.185/get_time.cgi';\" value=\"Get Time\"><br>"

"       <input type=\"number\" id = \"set_hours\" min=\"0\" max=\"23\" placeholder=\"Hours\"><tr>"
"       <input type=\"number\" id = \"set_minutes\" min=\"0\" max=\"59\" placeholder=\"Minutes\"><tr>"
"       <input type=\"number\" id = \"set_seconds\" min=\"0\" max=\"59\" placeholder=\"Seconds\"><tr>"
"  	<input type=\"submit\" onclick=\"set_time()\" value=\"Set Time\">"

"    	<script type=\"text/javascript\">"
"	function set_time() {" 
"  		var hours = document.getElementById(\"set_hours\").value;"
"  		var minutes = document.getElementById(\"set_minutes\").value;"
"  		var seconds = document.getElementById(\"set_seconds\").value;"

"  		window.location = \"http://192.168.105.185/set_time.cgi?\"+hours+\":\"+minutes+\":\"+seconds"
"	}"
"	</script>"


// IMMEDIATE SENSOR OPTIONS

"<br>"
"<br>"
"IMMEDIATE SENSOR OPTIONS: Select sensor number and click corresponding button"
"<br>"

"  	<select id=\"select_sensor_immediate\">"
"    	<option value=\"1\">1</option>"
"    	<option value=\"2\">2</option>"
"    	<option value=\"3\">3</option>"
"    	<option value=\"4\">4</option>"
"  	</select><tr>"

"	<input type=\"submit\" onclick=\"toggle_sensor_immediate('E')\" value=\"Enable\"><tr>"
"	<input type=\"submit\" onclick=\"toggle_sensor_immediate('D')\" value=\"Disable\">"

"    	<script type=\"text/javascript\">"
"	function toggle_sensor_immediate(choice) {" 
"  		var list = document.getElementById(\"select_sensor_immediate\");"
"		var sensor = list.options[list.selectedIndex].text;"
"  		window.location = \"http://192.168.105.185/toggle_sensor.cgi?\"+sensor+\",\"+choice"
"	}"
"	</script>"

// TIMED SENSOR OPTIONS

"<br>"
"<br>"
"TIMED SENSOR OPTIONS: Select sensor number, enter time and click corresponding button"
"<br>"

"  	<select id=\"select_sensor_timed\">"
"    	<option value=\"1\">1</option>"
"    	<option value=\"2\">2</option>"
"    	<option value=\"3\">3</option>"
"    	<option value=\"4\">4</option>"
"  	</select><tr>"

"       <input type=\"number\" id = \"set_hours_sensor\" min=\"0\" max=\"23\" placeholder=\"Hours\"><tr>"
"       <input type=\"number\" id = \"set_minutes_sensor\" min=\"0\" max=\"59\" placeholder=\"Minutes\"><tr>"
"       <input type=\"number\" id = \"set_seconds_sensor\" min=\"0\" max=\"59\" placeholder=\"Seconds\"><br>" 

"	<input type=\"submit\" onclick=\"toggle_sensor_timed('E')\" value=\"Enable\"><tr>"
"	<input type=\"submit\" onclick=\"toggle_sensor_timed('D')\" value=\"Disable\"><br>"

"    	<script type=\"text/javascript\">"
"	function toggle_sensor_timed(choice) {" 
"  		var list = document.getElementById(\"select_sensor_timed\");"
"		var sensor = list.options[list.selectedIndex].text;"
"  		var hours = document.getElementById(\"set_hours_sensor\").value;"
"  		var minutes = document.getElementById(\"set_minutes_sensor\").value;"
"  		var seconds = document.getElementById(\"set_seconds_sensor\").value;"
"  		window.location = \"http://192.168.105.185/timed_toggle.cgi?\"+sensor+\",\"+choice+\",\"+hours+\":\"+minutes+\":\"+seconds"
"	}"
"	</script>"


// BUTTON EMULATORS

"<br>"
"BUTTON EMULATOR: Select and option and press the relevant button"
"<br>"

"	<input type=\"submit\" onclick=\"toggle_alarm()\" value=\"Toggle Alarm\"><tr>"
"	<input type=\"submit\" onclick=\"hush_alarm()\" value=\"Hush Alarm\"><br>"

"    	<script type=\"text/javascript\">"
"	function toggle_alarm() {" 
"  		window.location = \"http://192.168.105.185/toggle_alarm.cgi\""
"	}"
"	</script>"

"    	<script type=\"text/javascript\">"
"	function hush_alarm() {" 
"  		window.location = \"http://192.168.105.185/hush_alarm.cgi\""
"	}"
"	</script>"

"</body>"
"</html>";

const TFS_DIR_ENTRY static_data[] = {{"/index.html", 0, http_refresh_text, sizeof(http_refresh_text)}, {0,0,0,0}};
//response buffer
char buffer[100] = {0};
//time formatting variable
int time[3]= {0,0,0};
HMI_CLIENT_STRUCT_PTR hmi_client;

////////// NON-CALLBACK FUNCTIONS

// triggers enabling / disabling of a sensor's timer
// Action = 1 => Enable and reset enabling timer for the specified sensor
// Action = 0 => Disable and reset disabling timer for the specified sensor
void expire_sensor_timer(int index, int action) {
	//if it is an enable event to be triggered
	if(action == 1)	{
		//reset the timed event time
		sensor_enable_time[index] = -1;
		//if not already enabled
		if (!sensors_enabled[index]) {
			//enable and set initial state
			sensors_enabled[index] = 1;
			sensors_triggered[index] = 0;
			//if the alarm is ON, also light the LED
			if(alarm_on) {
				btnled_set_value(hmi_client, HMI_GET_LED_ID(index + 1), HMI_VALUE_ON);
			}
		}
	//if it is a disable event to be triggered
	} else if (action == 0) {
		//reset the timed event time
		sensor_disable_time[index] = -1;
		//if not already disabled
		if (sensors_enabled[index]) {
			//disable and set initial state
			sensors_enabled[index] = 0;
			sensors_triggered[index] = 0;
			//if the alarm is ON, turn off the LED
			if(alarm_on) {
				btnled_set_value(hmi_client, HMI_GET_LED_ID(index + 1), HMI_VALUE_OFF);
			}
		}
	}
}

// implements the timed sensor enable/disable functionality
// constantly polls the current time to not miss any events
void timed_sensor_toggle() {
	int index, seconds;	
	RTC_TIME_STRUCT curr_time;
	_rtc_get_time(&curr_time);

	//normalize it to the time in a day
	seconds = curr_time.seconds % 86400;

	for (index = 0; index < 4; index ++) {
		int is_enable_first = sensor_enable_time[index] <= sensor_disable_time[index];
		//do not disable or enable unless time is right and not scheduled for time = -1
		int is_enable_time_valid = sensor_enable_time[index] <= seconds && sensor_enable_time[index] != -1;
		int is_disable_time_valid = sensor_disable_time[index] <= seconds && sensor_disable_time[index] != -1;
		//if we first enable then disable, based on where actions are on timeline
		if(is_enable_first) {
			//first enable then disable
			if (is_enable_time_valid)
				expire_sensor_timer(index, 1);
			if (is_disable_time_valid)
				expire_sensor_timer(index, 0);
		} else { //first disable then enable
			if (is_disable_time_valid)
				expire_sensor_timer(index, 0);
			if (is_enable_time_valid)
				expire_sensor_timer(index, 1);
		}
	}
}

// splits the time returned by '_rtc_get_time' into hours/minutes/seconds
void get_time(int total_seconds) {
	time[0] = total_seconds % 60;
	time[1] = (total_seconds / 60 ) % 60;
	time[2] = (total_seconds / 3600) % 24;
}

// converts the specified time in 'HH:MM:SS' to a format parsable by '_rtc_set_time'
int set_time() {
	int result = 0;
	result += time[0];
	result += time[1] * 60;
	result += time[2] * 3600;
	return result;
}

// performs a check whether 'time' specifies a valid time format
// time[2] - hours
// time[1] - minutes
// time[0] - seconds
int time_valid() {
	//valid format is 0-23 hours, 0-59 minutes, 0-59 seconds
	if (time[2] > 23 || time[2] < 0 || time[1] > 59 || time[1] < 0 || time[0] > 59 || time[0] < 0) {
		return 0;
	}
	return 1;
}

// toggle the state of any triggered LEDs
void flash_leds() {
	int i;
	for(i = 0; i < 4; i++) {
		// toggle triggered sensors
		if(sensors_triggered[i]) {
			btnled_toogle(hmi_client, HMI_GET_LED_ID(i+1));
		}
	}
}

////////// BUTTON CALLBACK FUNCTIONS

// Trigger function - button callback, accessed by pressing any of the buttons 1-4
// only sets the triggered flag, to be used by the flash_leds function
void trigger_sensor(void *ptr) {
	if (alarm_on) {
		int btn_num = *((int*) ptr);
		if(sensors_enabled[btn_num-1]) {
			sensors_triggered[btn_num-1] = 1;
		}
	}
}

// Alarm function - button callback, accessed by pressing button 5
// switches LEDs on/off unless individual sensors are disabled
// also resets the triggered and hushed states
void toggle_alarm(void *ptr) {
	int i;
	for(i = 0; i < 4; i++) {
		//if the alarm is OFF, turn on all the enabled LEDs	
		if(!alarm_on && sensors_enabled[i]) {
			btnled_set_value(hmi_client, HMI_GET_LED_ID(i+1), HMI_VALUE_ON);
		//if the alarm is ON, turn all the LEDs off
		} else {
			btnled_set_value(hmi_client, HMI_GET_LED_ID(i+1), HMI_VALUE_OFF);
		}
		sensors_triggered[i] = 0;
	}
	alarm_hushed = 0;
	alarm_on = (alarm_on) ? 0 : 1;
}

// Hush function - button callback, accessed by pressing button 6
// toggles enabling/disabling access to the flash_leds function 
void toggle_hush(void *ptr) {
	//only do anything if the alarm is on
	if(alarm_on) {
		if(!alarm_hushed) {
			int i;
			for(i = 0; i < 4; i++) {
				if(sensors_enabled[i]) {
					// make sure the toggling does not leave the LED off
					btnled_set_value(hmi_client, HMI_GET_LED_ID(i+1), HMI_VALUE_ON);
				}
			}
		}
		//toggle hushing
		alarm_hushed = (alarm_hushed) ? 0 : 1;
	}
}

////////// HTML CALLBACK FUNCTIONS

// HTML callback to set time - specifies the 'set_time.cgi' subpage
// this involves reading the time as 'HH:MM:SS' from the URL and setting it as current time
_mqx_int set_time_callback(HTTPD_SESSION_STRUCT *session) {
	RTC_TIME_STRUCT the_new_time;
	
	//get time values for hours, minutes, seconds
	int valid_vars = sscanf(session->request.urldata, "%u:%u:%u" , &time[2], &time[1], &time[0]);
	//make sure format is correct
	if (valid_vars == 3 && time_valid()) {
		//set time to new values
		the_new_time.seconds = set_time();
		_rtc_set_time(&the_new_time);
		//inform the user
		sprintf(buffer, "Time successfully set to %u:%u:%u\n", time[2], time[1], time[0]);
	} else {
		//inform the user of a problem
		sprintf(buffer, "Wrong time format!");
	}
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

// HTML callback to get time - specifies the 'get_time.cgi' subpage
// this involves returning the current time, converted with the 'get_time' method
_mqx_int get_time_callback(HTTPD_SESSION_STRUCT *session) {
	RTC_TIME_STRUCT curr_time;
	_rtc_get_time(&curr_time);
	get_time(curr_time.seconds);
	//send the current time
	sprintf(buffer, "%u:%u:%u\n", time[2], time[1], time[0]);

	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

// HTML callback for immediate sensor control - specifies the 'toggle_sensor.cgi' subpage
// this involves reading an LED id number and the desired operation from the URL
_mqx_int toggle_sensor_callback(HTTPD_SESSION_STRUCT *session) {
	int sensor;
	char enabled;
	//get the variables from the URL
	int valid_vars = sscanf(session->request.urldata, "%u,%c", &sensor, &enabled);
	//check that it is a valid action and a valid sensor index
	if (valid_vars == 2 && (enabled == 'E' || enabled == 'D') && sensor > 0 && sensor < 5) {
		//enable sensor indicated
		if (enabled == 'D') {
			sensors_enabled[sensor-1] = 0;
			sensors_triggered[sensor-1] = 0;
			btnled_set_value(hmi_client, HMI_GET_LED_ID(sensor), HMI_VALUE_OFF);
		} 
		//disable sensor indicated
		else if (!sensors_enabled[sensor-1]) {
			sensors_enabled[sensor-1] = 1;
			sensors_triggered[sensor-1] = 0;
			if(alarm_on) {
				btnled_set_value(hmi_client, HMI_GET_LED_ID(sensor), HMI_VALUE_ON);
			}
		}
		//inform the user
		sprintf(buffer, "Sensor %u now %s\n", sensor, (sensors_enabled[sensor-1]) ? "ENABLED" : "DISABLED" );
	} else {
		//inform the used there was a problem
		sprintf(buffer, "Wrong input format!");
	}
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;	
}

// HTML callback for timed sensor control - specifies the 'timed_toggle.cgi' subpage
// this involves reading an LED id number, the desired operation, and the time from the URL
// only sets the desired times, actual operation done by the timed_sensor_toggle method
// note that this is a ONE-TIME ALARM: timers are reset once current time exceeds enable/disable times
// hence if current time is already higher, the function works as an instant one-time toggle
_mqx_int timed_toggle_callback(HTTPD_SESSION_STRUCT *session) {
	int sensor;
	char enabled;
	//get the variables from the URL
	int valid_vars = sscanf(session->request.urldata, "%u,%c,%u:%u:%u", &sensor, &enabled, &time[2], &time[1], &time[0]);
	//check if the action and sensor index are valid
	if (valid_vars == 5 && time_valid() && (enabled == 'E' || enabled == 'D') && sensor > 0 && sensor < 5) {
		int total_seconds = set_time();
		//inform the user
		sprintf(buffer, "Sensor %u will be %s at %u:%u:%u", sensor, (enabled == 'E') ? "ENABLED" : "DISABLED", time[2], time[1], time[0]);	
		//set enable timer
		if(enabled == 'E') {
			sensor_enable_time[sensor-1] = total_seconds;
		} 
		//set disable timer
		else {
			sensor_disable_time[sensor-1] = total_seconds;
		}
	} else {
		//inform the user there was a problem
		sprintf(buffer, "Wrong input format!");
	}
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

// HTML callback alarm system (power) control - specifies the 'toggle_alarm.cgi' subpage
// this involves toggling the alarm power (on / off), as is done via the relevant button
_mqx_int toggle_alarm_callback(HTTPD_SESSION_STRUCT *session) {
	//inform the user
	sprintf(buffer, "Alarm system turned %s", (alarm_on) ? "OFF" : "ON");
	//call local procedure, also linked to corresponding button
	toggle_alarm(NULL);
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

// HTML callback alarm system (hush) control - specifies the 'toggle_hush.cgi' subpage
// this involves toggling the alarm hush (hushed / not hushed), as is done via relevant the button
_mqx_int hush_alarm_callback(HTTPD_SESSION_STRUCT *session) {
	//inform the user
	sprintf(buffer, "Alarm system %s", (alarm_hushed) ? "UN-HUSHED" : "HUSHED");
	//call local procedure, also linked to corresponding button
	toggle_hush(NULL);
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

// HTML callback to list all current timers - used only for debugging purposes
// Only manually accessible through 'get_timers.cgi'
// Value 0:0:4294967295 means that no timer is set
_mqx_int get_timers_callback(HTTPD_SESSION_STRUCT *session) {
	int i;
	int longbuf_end = 0;
	char longbuf[300] = {0};
	for (i = 0; i < 4; i++) {
		get_time(sensor_enable_time[i]);
		longbuf_end += sprintf(longbuf+longbuf_end, "Sensor %u enabled at %u:%u:%u\n", i+1, time[2], time[1], time[0]);
		get_time(sensor_disable_time[i]);
		longbuf_end += sprintf(longbuf+longbuf_end, "Sensor %u disabled at %u:%u:%u\n", i+1, time[2], time[1], time[0]);
	}
	httpd_sendstr(session->sock, longbuf);
	return session->request.content_len;
}

// list of CGIs, i.e. action webpages
static HTTPD_CGI_LINK_STRUCT http_cgi_params[] = {
						   {"get_time", get_time_callback}, 
						   {"set_time", set_time_callback}, 
						   {"toggle_sensor", toggle_sensor_callback},
						   {"timed_toggle", timed_toggle_callback},
						   {"get_timers", get_timers_callback},
						   {"toggle_alarm", toggle_alarm_callback},
						   {"hush_alarm", hush_alarm_callback},
						   {0,0}
						 };

// initially set the LEDs to OFF and initialize the callbacks
void init_leds(int *array) {
	btnled_set_value(hmi_client, HMI_LED_1, HMI_VALUE_OFF);
	btnled_set_value(hmi_client, HMI_LED_2, HMI_VALUE_OFF);
	btnled_set_value(hmi_client, HMI_LED_3, HMI_VALUE_OFF);
	btnled_set_value(hmi_client, HMI_LED_4, HMI_VALUE_OFF);
	btnled_add_clb(hmi_client, HMI_BUTTON_1, HMI_VALUE_PUSH, trigger_sensor, array);
	btnled_add_clb(hmi_client, HMI_BUTTON_2, HMI_VALUE_PUSH, trigger_sensor, array+1);
	btnled_add_clb(hmi_client, HMI_BUTTON_3, HMI_VALUE_PUSH, trigger_sensor, array+2);
	btnled_add_clb(hmi_client, HMI_BUTTON_4, HMI_VALUE_PUSH, trigger_sensor, array+3);
	btnled_add_clb(hmi_client, HMI_BUTTON_5, HMI_VALUE_PUSH, toggle_alarm, NULL);
	btnled_add_clb(hmi_client, HMI_BUTTON_6, HMI_VALUE_PUSH, toggle_hush, NULL);
}

void Main_task(uint_32 initial_data) {
	HTTPD_ROOT_DIR_STRUCT http_root_dir[] = {{"", "tfs:"}, { 0,0 }};
	HTTPD_STRUCT *http_server;
	int array[4] = {1,2,3,4};

	int counter = 0;
	hmi_client = _bsp_btnled_init();
	init_leds(array);

	rtcs_init();
	_rtc_init(RTC_INIT_FLAG_ENABLE);
	_io_tfs_install("tfs:", static_data);
	
	http_server = httpd_server_init_af(http_root_dir, "\\index.html", AF_INET);
	HTTPD_SET_PARAM_CGI_TBL(http_server, http_cgi_params);
	httpd_server_run(http_server);
	
	while(1) {
		// flash triggered LEDs every 100 clock cycles
		if (counter++ == 100) {
			counter = 0;	
			//only flash LEDs if the alarm is not hushed
			if(!alarm_hushed) {
				flash_leds();
			}
		}
		//check if any of the timed toggles are due to be activated
		timed_sensor_toggle();
		btnled_poll(hmi_client);
		ipcfg_task_poll();
	}
}

/* EOF */

