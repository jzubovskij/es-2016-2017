/**HEADER*******************************************************************
 * * 
 * * Copyright (c) 2008 Freescale Semiconductor;
 * * All Rights Reserved
 * *
 * * Copyright (c) 1989-2008 ARC International;
 * * All Rights Reserved
 * *
 * **************************************************************************** 
 * *
 * * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
 * * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
 * * IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * * THE POSSIBILITY OF SUCH DAMAGE.
 * *
 * ****************************************************************************
 * *
 * * Comments:
 * *
 * *   This file contains main initialization for your application
 * *   and infinite loop
 * *
 * *END************************************************************************/

#include "main.h"
#include <tfs.h>
#include <httpd.h>
#include <httpd_types.h>


#if defined(APPLICATION_HAS_SHELL) && (!SHELLCFG_USES_RTCS)
#error This application requires SHELLCFG_USES_RTCS defined non-zero in user_config.h. Please recompile libraries with this option if any Ethernet interface is available.
#endif

TASK_TEMPLATE_STRUCT MQX_template_list[] =
{
/*  Task number, Entry point, Stack, Pri, String, Auto? */
   {MAIN_TASK,   Main_task,   2000,  9,   "main", MQX_AUTO_START_TASK},
   {0,           0,           0,     0,   0,      0,                 }
};

/*TASK*-----------------------------------------------------------------
 * *
 * * Function Name  : Main_task
 * * Comments       :
 * *    This task initializes MFS and starts SHELL.
 * *
 * *END------------------------------------------------------------------*/

unsigned char http_refresh_text[] = "Hello World!";
const TFS_DIR_ENTRY static_data[] = {{"/index.html", 0, http_refresh_text, sizeof(http_refresh_text)}, {0,0,0,0}};
char buffer[32];
int time[3];
HMI_CLIENT_STRUCT_PTR hmi_client;

void get_time(int total_seconds) {
	time[0] = total_seconds % 60;
	time[1] = (total_seconds / 60 ) % 60;
	time[2] = (total_seconds / 3600) % 24;
}

int set_time() {
	int result = 0;
	result += time[0];
	result += time[1] * 60;
	result += time[2] * 3600;
	return result;
}

_mqx_int led_callback(HTTPD_SESSION_STRUCT *session) {
	int led = atoi(session->request.urldata);
	if (led > 0 && led < 5) {
		btnled_toogle(hmi_client, HMI_GET_LED_ID(led));
		sprintf(buffer, "LED %u successfully toggled", led);
	} else {
		sprintf(buffer, "Wrong LED number formatting!");
	}
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

_mqx_int set_time_callback(HTTPD_SESSION_STRUCT *session) {
	RTC_TIME_STRUCT the_new_time;
	int valid_vars = sscanf(session->request.urldata, "%u:%u:%u" , &time[2], &time[1], &time[0]);
	if (valid_vars == 3 && time[2] < 24 && time[2] > -1 && time[1] < 60 && time[1] > -1 && time[0] < 60 && time[0] > -1) {
		the_new_time.seconds = set_time();
		_rtc_set_time(&the_new_time);
		sprintf(buffer, "Time successfully set to %u:%u:%u\n", time[2], time[1], time[0]);
	} else {
		sprintf(buffer, "Wrong time format!");
	}
	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

_mqx_int get_time_callback(HTTPD_SESSION_STRUCT *session) {
	RTC_TIME_STRUCT curr_time;
	_rtc_get_time(&curr_time);
	get_time(curr_time.seconds);
	sprintf(buffer, "%u:%u:%u\n", time[2], time[1], time[0]);

	httpd_sendstr(session->sock, buffer);
	return session->request.content_len;
}

static HTTPD_CGI_LINK_STRUCT http_cgi_params[] = {
						   {"led", led_callback}, 
						   {"get_time", get_time_callback},
						   {"set_time", set_time_callback},
						   {0,0}
						};

void Main_task(uint_32 initial_data) {
	HTTPD_ROOT_DIR_STRUCT http_root_dir[] = {{"", "tfs:"}, { 0,0 }};
	HTTPD_STRUCT *http_server;

	hmi_client = _bsp_btnled_init();
	rtcs_init();
	_rtc_init(RTC_INIT_FLAG_ENABLE);
	_io_tfs_install("tfs:", static_data);
	
	http_server = httpd_server_init_af(http_root_dir, "\\index.html", AF_INET);
	HTTPD_SET_PARAM_CGI_TBL(http_server, http_cgi_params);
	httpd_server_run(http_server);

	while(1) ipcfg_task_poll();
}
/* EOF */

