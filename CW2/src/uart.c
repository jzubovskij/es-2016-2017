#include "uart.h"
#include "MK70F12.h"

void uart_init()
{
	SIM_SCGC4 |= SIM_SCGC4_UART2_MASK; // Enabling clock gating to the UART port

	PORTE_PCR16 =  PORT_PCR_MUX(3) | PORT_PCR_DSE_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; // TX
	PORTE_PCR17 =  PORT_PCR_MUX(3) | PORT_PCR_DSE_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; // RX

	// UART baud rate [115200] = UART module clock [50 MHz] / (16 × (SBR[12:0] + BRFD))
	// So SBR+BRFD = 27.12673.. ~ 27.125
	UART2_BDH = 0;		// high 5 bits of baud rate
	UART2_BDL = 27;		// low 8 bits of baud rate
	UART2_C4 |= (1 << 2);	// bits 4-0 enable baud rate fine adjust (BRFA)

	UART2_C2 |= (1 << 7); // enable transmitter change interrupts
	UART2_C2 |= (1 << 5); // enable receiver change interrupts
	UART2_C2 |= (1 << 3); // enable transmitter
	UART2_C2 |= (1 << 2); // enable receiver
}

char uart_get()
{	
	while (!(UART2_S1 & UART_S1_RDRF_MASK)); // Get only if something received
	return UART2_D;
}

void uart_put(char x)
{
	while (!(UART2_S1 & UART_S1_TDRE_MASK)); // Send only if there is space
	UART2_D = x;
}
