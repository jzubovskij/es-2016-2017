#!/bin/sh

if [ "$1 " == " " ]; then
	echo "Usage: ./all.sh [.wav file]"
	exit
fi;

# make spectrogram
sox $1 -n spectrogram
mv spectrogram.png spectrogram_orig.png

# extract file name
IFS='.' tokens=( $1 )
filename=${tokens[0]}

# convert to raw, pass to board, convert back
./wav2raw.sh $filename.wav
./pipe.sh ${filename}.raw ${filename}_out.raw
./raw2wav.sh ${filename}_out.raw

# make new spectrogram
sox ${filename}_out.wav -n spectrogram
mv spectrogram.png spectrogram_new.png

