/*
 * Bare metal template created by Harry Wagstaff
 * Based on Freescale Codewarrior Bareboard project template
 * Edited by Stan Manilov
 */

#include "MK70F12.h"

#include "led.h"
#include "button.h"
#include "uart.h"
#include "filter.h"

int filter_select = 1; // Set filter 1 as default filter selection

// __init_hardware is called by the Freescale __thumb_startup function (see
// vectors.c)
void __init_hardware()
{
	// Disable the Watchdog module. This module is designed to reset
	// the board in case it crashes for some reason. We don't need it,
	// so we disable it here.
	WDOG_UNLOCK = 0xC520;
	WDOG_UNLOCK = 0xD928;
	WDOG_STCTRLH = 0xD2;

	// Configure the MCG - set up clock dividers on
	SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV2(0) | SIM_CLKDIV1_OUTDIV3(1) | SIM_CLKDIV1_OUTDIV4(1);
	MCG_C1 = MCG_C1_CLKS(2);

	led_init();
	button_init();
	uart_init();
	
	// Set CPACR (coprocessor access) to 3 (0b11) - full access to FPU;
	SCB_CPACR = SCB_CPACR_CP10(3) | SCB_CPACR_CP11(3);
	// FPSCR, FPCCR used for more indepth setting and FPDSCR is defaults
}

// Light the LED of selected filter
void light_filter_led(filter_select)
{
	switch(filter_select) {
		case 1: led_on(LED_BLUE);
			led_off(LED_GREEN);
			led_off(LED_YELLOW);
			led_off(LED_RED);
			break;
		case 2: led_off(LED_BLUE);
			led_on(LED_GREEN);
			led_off(LED_YELLOW);
			led_off(LED_RED);
			break;
		case 3: led_off(LED_BLUE);
			led_off(LED_GREEN);
			led_on(LED_YELLOW);
			led_off(LED_RED);
			break;
		case 4: led_off(LED_BLUE);
			led_off(LED_GREEN);
			led_off(LED_YELLOW);
			led_on(LED_RED);
			break;
	}
}

void main()
{
	unsigned char ch_in; // CHAR_SIZE is 8 bits, same as our data
	signed char ch_s;
	float fl_in = 0, fl_out = 0;

	filterType *filter = filter_create();

	while(1)
	{
		// Receive unsigned char
		ch_in = uart_get();

		// Convert char to signed
		ch_s = ch_in;
		// Get corresponding float value
		fl_in = ch_s / 128.0f;
		// Pass to filter, get float back
		filter_filterBlock(filter, &fl_in, &fl_out, filter_select);
		// Put float back in unsigned char
		ch_in = (int) (fl_out * 128);

		// Send unsigned char back
		uart_put(ch_in);
	}
}
