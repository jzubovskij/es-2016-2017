/******************************* SOURCE LICENSE *********************************
Copyright (c) 2015 MicroModeler.

A non-exclusive, nontransferable, perpetual, royalty-free license is granted to the Licensee to 
use the following Information for academic, non-profit, or government-sponsored research purposes.
Use of the following Information under this License is restricted to NON-COMMERCIAL PURPOSES ONLY.
Commercial use of the following Information requires a separately executed written license agreement.

This Information is distributed WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

******************************* END OF LICENSE *********************************/

// A commercial license for MicroModeler DSP can be obtained at http://www.micromodeler.com/launch.jsp

#ifndef FILTER_H_
#define FILTER_H_

typedef struct
{
	float state[16];
	float output;
} filterType;

typedef struct
{
	float *pInput;
	float *pOutput;
	float *pState;
	float *pCoefficients;
} filter_executionState;

filterType *filter_create( void );
void filter_destroy( filterType *pObject );
void filter_init( filterType * pThis );
void filter_reset( filterType * pThis );

void filter_filterBlock( filterType * pThis, float * pInput, float * pOutput, int select);
void filter_filterBiquad( filter_executionState * pExecState );

#endif // FILTER_H_
	

